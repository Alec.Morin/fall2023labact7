package lab7;

public class BagelSandwich implements ISandwich{
    private String filling;

    public BagelSandwich(){
        this.filling = "";
    }

    @Override
    public String getFilling() {
        return this.filling;
    }

    @Override
    public void addFilling(String filling) {
       this.filling += filling;
    }

    @Override
    public boolean isVegetarian() {
        throw new UnsupportedOperationException("Unimplemented method 'isVegetarian'");
    }


}