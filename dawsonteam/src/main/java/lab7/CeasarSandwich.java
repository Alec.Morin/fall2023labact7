package lab7;

public class CeasarSandwich extends VegetarianSandwich{

    public CeasarSandwich(){
        super.addFilling("ceasar dressing");
    }

    @Override
    public String getProtein() {
        return "anchovies";
    }

    @Override
    public boolean isVegetarian() {
        return false;
    }
    
}
