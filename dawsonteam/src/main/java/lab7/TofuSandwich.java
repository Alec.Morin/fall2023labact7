package lab7;

public class TofuSandwich extends VegetarianSandwich{

    public TofuSandwich(){
        super.addFilling("Tofu");
    }

    @Override
    public String getProtein(){
        return "Tofu";
    }
}
