package lab7;

public interface ISandwich{

    String getFilling();
    void addFilling(String filling);
    boolean isVegetarian();
    
}