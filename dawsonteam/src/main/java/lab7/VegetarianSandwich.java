package lab7;

public abstract class VegetarianSandwich implements ISandwich{

    private String filling;

    public VegetarianSandwich(){
        this.filling = "";
    }

    @Override
    public String getFilling() {
        return this.filling;
    }

    @Override
    public void addFilling(String filling) {
        this.filling += " " + filling;
    }

    @Override
    public boolean isVegetarian() {
        return true;
    }

    public boolean isVegan(){
        if (this.filling.contains("cheese") || this.filling.contains("egg")){
            return false;
        }
        return true;
    }

    public abstract String getProtein();
}