package lab7;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class BagelSandwichTest{
    
    @Test
    public void testConstructor(){      
        BagelSandwich mybagel = new BagelSandwich();
        assertTrue(true);
    }

    @Test
    public void testGetFilling(){
        BagelSandwich mybagel = new BagelSandwich();
        assertEquals("Testing getFilling. Should return an empty string", "", mybagel.getFilling());
    }

    @Test
    public void testAddFilling(){
        BagelSandwich mybagel = new BagelSandwich();
        mybagel.addFilling("nutella");
        assertEquals("Testing addFilling. Should return nutella", "nutella", mybagel.getFilling());
    }

    @Test
    public void testIsVegetarian(){
        BagelSandwich mybagel = new BagelSandwich();
       try{
        mybagel.isVegetarian();
        fail("isVegetarian shoudl throw an error");
       }catch(UnsupportedOperationException UOE){
        
       }
    }


}
